
Done:
1) Implemented Search functionality . The default search text is set to texas instead of blank screen. 
2) only implemented onclick on search icon , needs some tweaks to make imedone working.
3) USed Paging library for pagination.
4) Designed the app in MVVM arch using Retrofit (networking), ViewModel ,LiveData , Rx java ,dagger.
5) used Room to store and retrive comments.
6) handled orientation.


Needs Work:

1)adjusting the paddings
2)error dialog on network unavialbility.
3)making RecyclerView even more faster . 
4)adding unit test cases.

