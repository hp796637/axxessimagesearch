package com.dev.meet.axxessproject.models;

import java.util.List;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ImagesItem {


    @SerializedName("link")
    private String link;

    @SerializedName("description")
    private String description;

    @SerializedName("section")
    private Object section;

    @SerializedName("title")
    private Object title;

    @SerializedName("type")
    private String type;

    @SerializedName("id")
    private String id;


    public void setLink(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setSection(Object section) {
        this.section = section;
    }

    public Object getSection() {
        return section;
    }

    public void setTitle(Object title) {
        this.title = title;
    }

    public Object getTitle() {
        return title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return
                "ImagesItem{" +
                        ",link = '" + link + '\'' +
                        ",description = '" + description + '\'' +
                        ",section = '" + section + '\'' +
                        ",title = '" + title + '\'' +
                        ",type = '" + type + '\'' +
                        ",id = '" + id + '\'' +
                        "}";
    }
}