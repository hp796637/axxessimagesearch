package com.dev.meet.axxessproject.Constants;

/**
 * Created by harishpolusani on 4/8/19.
 */

public class Constants {

    public static final String id = "PRIMARY_KEY";

    public static final String URL = "IMAGE_URL";

    public static final String TITLE="TITLE";
}
