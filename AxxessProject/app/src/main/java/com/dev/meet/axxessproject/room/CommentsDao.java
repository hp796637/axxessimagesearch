package com.dev.meet.axxessproject.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import io.reactivex.Single;

/**
 * Created by harishpolusani on 4/8/19.
 */

@Dao
public interface CommentsDao {


    @Insert
    void insertAll(Comments... comments);

    @Delete
    void delete(Comments comment);

    @Update
    void update(Comments comment);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Comments comments);

    @Query("DELETE FROM comment")
    public void nukeTable();

    @Query("SELECT * FROM comment WHERE id =:taskId")
    Single<Comments> getComment(String taskId);


}
