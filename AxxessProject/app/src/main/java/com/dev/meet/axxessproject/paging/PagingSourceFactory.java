package com.dev.meet.axxessproject.paging;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import com.dev.meet.axxessproject.models.DataItem;
import com.dev.meet.axxessproject.repository.PictureRepository;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by harishpolusani on 4/7/19.
 */

public class PagingSourceFactory extends DataSource.Factory<Integer, DataItem> {


    private MutableLiveData<PictureDataSourceClass> liveData;
    private PictureRepository repository;
    private CompositeDisposable compositeDisposable;


    public PagingSourceFactory(PictureRepository repository, CompositeDisposable compositeDisposable) {
        this.repository = repository;
        this.compositeDisposable = compositeDisposable;
        liveData = new MutableLiveData<>();
    }

    public MutableLiveData<PictureDataSourceClass> getMutableLiveData() {
        return liveData;
    }

    @Override
    public DataSource<Integer, DataItem> create() {
        PictureDataSourceClass pictureDataSourceClass = new PictureDataSourceClass(repository, compositeDisposable);
        liveData.postValue(pictureDataSourceClass);
        return pictureDataSourceClass;
    }
}
