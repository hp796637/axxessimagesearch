package com.dev.meet.axxessproject.models;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import java.util.List;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataItem {

    @SerializedName("ad_type")
    private int adType;

    @SerializedName("link")
    private String link;

    @SerializedName("description")
    private String description;

    @SerializedName("privacy")
    private String privacy;

    @SerializedName("section")
    private String section;

    @SerializedName("title")
    private String title;

    @SerializedName("id")
    private String id;

    @SerializedName("images")
    private List<ImagesItem> images;

    @SerializedName("type")
    private String type;

    public void setAdType(int adType) {
        this.adType = adType;
    }

    public int getAdType() {
        return adType;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSection() {
        return section;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }


    public void setImages(List<ImagesItem> images) {
        this.images = images;
    }

    public List<ImagesItem> getImages() {
        return images;
    }


    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }


    @Override
    public String toString() {
        return
                "DataItem{" +
                        ",ad_type = '" + adType + '\'' +
                        ",link = '" + link + '\'' +
                        ",description = '" + description + '\'' +
                        ",privacy = '" + privacy + '\'' +
                        ",section = '" + section + '\'' +
                        ",title = '" + title + '\'' +
                        ",id = '" + id + '\'' +
                        ",images = '" + images + '\'' +
                        ",type = '" + type + '\'' +
                        "}";
    }


    public static DiffUtil.ItemCallback<DataItem> DIFF_CALLBACK = new DiffUtil.ItemCallback<DataItem>() {
        @Override
        public boolean areItemsTheSame(@NonNull DataItem oldResponse, @NonNull DataItem newResponse) {
            return oldResponse.getId().equals(newResponse.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull DataItem oldResponse, @NonNull DataItem newResponse) {
            return oldResponse.equals(newResponse);
        }
    };

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        DataItem imageData = (DataItem) obj;
        return imageData.getId().equals(this.getId());
    }
}