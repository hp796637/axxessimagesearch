package com.dev.meet.axxessproject.repository;

import android.content.Context;

import com.dev.meet.axxessproject.models.Response;
import com.dev.meet.axxessproject.network.RetrofitApiInterface;
import com.dev.meet.axxessproject.room.AppDataBase;
import com.dev.meet.axxessproject.room.Comments;
import com.dev.meet.axxessproject.room.CommentsDao;

import javax.inject.Inject;

import io.reactivex.Single;


/**
 * Created by harishpolusani on 4/5/19.
 */

public class PictureRepository {

    @Inject
    CommentsDao commentsDao;


    @Inject
    AppDataBase appDataBase;
    private RetrofitApiInterface retrofitApiInterface;

    public PictureRepository(RetrofitApiInterface retrofitApiInterface) {
        this.retrofitApiInterface = retrofitApiInterface;
    }

    public Single<Response> fetchPicture(int page, String search) {
        return retrofitApiInterface.fetchImages(page, search);
    }

    public void addComment(Context context, String comment, String id) {

        Comments comments = new Comments();
        comments.setComment(comment);
        comments.setId(id);
        AppDataBase.getAppDatabase(context).commentsDao().insert(comments);
    }

    public Single<Comments> getComment(Context context, String id) {
        return AppDataBase.getAppDatabase(context).commentsDao().getComment(id);
    }

}
