package com.dev.meet.axxessproject.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.meet.axxessproject.Constants.Status;
import com.dev.meet.axxessproject.Constants.UrlUtils;
import com.dev.meet.axxessproject.PictureSearchApplication;
import com.dev.meet.axxessproject.R;
import com.dev.meet.axxessproject.adapter.PictureGridAdapter;
import com.dev.meet.axxessproject.viewmodel.PictureActivityViewModel;
import com.dev.meet.axxessproject.viewmodel.PictureViewModelFactory;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PictureActivity extends AppCompatActivity {


    private static final int NUMBER_OF_COLUMNS = 4;

    @Inject
    PictureViewModelFactory viewModelFactory;

    private PictureActivityViewModel viewModel;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.search_field)
    EditText editText;

    @BindView(R.id.search_icon)
    ImageView imageView;

    @BindView(R.id.pb)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pictures_layout);
        ButterKnife.bind(this);
        ((PictureSearchApplication) getApplication()).getAppComponent().inject(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PictureActivityViewModel.class);
        initView();
        loadRecyclerView();
    }

    private void loadRecyclerView() {
        recyclerView.setLayoutManager(new GridLayoutManager(this, NUMBER_OF_COLUMNS));
        PictureGridAdapter pictureGridAdapter = new PictureGridAdapter();
        recyclerView.setAdapter(pictureGridAdapter);

        viewModel.getImages().observe(this, pictureGridAdapter::submitList);
    }

    public void initView() {
        viewModel.getProgressLoadStatus().observe(this, new Observer<Status>() {
            @Override
            public void onChanged(@Nullable Status status) {
                if (status == Status.LOADING) {
                    progressBar.setVisibility(View.VISIBLE);
                } else if (status == Status.LOADED) {
                    progressBar.setVisibility(View.GONE);
                } else if (status == Status.FAILED) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(PictureActivity.this, "failed to load images", Toast.LENGTH_SHORT).show();
                    //TODO Error Dialog
                } else if (status == Status.NO_DATA_FOUND) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(PictureActivity.this, "NO Images Found", Toast.LENGTH_SHORT).show();
                }

            }
        });
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    UrlUtils.SEARCH = editText.getText().toString().trim();
                    if (viewModel.getImages().getValue() != null)
                        viewModel.getImages().getValue().getDataSource().invalidate();
                    return true;
                }
                return false;
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UrlUtils.SEARCH = editText.getText().toString().trim();
                if (viewModel.getImages().getValue() != null)
                    viewModel.getImages().getValue().getDataSource().invalidate();
            }
        });
    }

}
