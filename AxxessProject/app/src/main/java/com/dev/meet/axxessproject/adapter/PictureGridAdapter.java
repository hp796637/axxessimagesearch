package com.dev.meet.axxessproject.adapter;

import android.app.Activity;
import android.arch.paging.PagedListAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.dev.meet.axxessproject.Constants.Constants;
import com.dev.meet.axxessproject.R;
import com.dev.meet.axxessproject.models.DataItem;
import com.dev.meet.axxessproject.models.ImagesItem;
import com.dev.meet.axxessproject.view.DetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by harishpolusani on 4/7/19.
 */

public class PictureGridAdapter extends PagedListAdapter<DataItem, PictureGridAdapter.PictureViewHolder> {


    public PictureGridAdapter() {
        super(DataItem.DIFF_CALLBACK);
    }


    @NonNull
    @Override
    public PictureGridAdapter.PictureViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.child_image_item, viewGroup, false);
        return new PictureViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PictureGridAdapter.PictureViewHolder viewHolder, int i) {
        List<ImagesItem> imagesItems = getItem(i).getImages();
        String imageUrl;
        if (imagesItems == null || imagesItems.size() == 0) {
            imageUrl = getItem(i).getLink();
        } else {
            imageUrl = imagesItems.get(0).getLink();
        }

        viewHolder.imageView.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), DetailActivity.class);
            intent.putExtra(Constants.id, getItem(i).getId());
            intent.putExtra(Constants.URL, imageUrl);
            intent.putExtra(Constants.TITLE,getItem(i).getTitle());
            view.getContext().startActivity(intent);
        });
        Glide.with(viewHolder.imageView.getContext())
                .load(imageUrl).thumbnail(0.1f).placeholder(R.drawable.ic_image_black_place_holder).fitCenter().transition(DrawableTransitionOptions.withCrossFade()).apply(new RequestOptions().override(100, 100)).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(viewHolder.imageView);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    static class PictureViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_view)
        ImageView imageView;

        public PictureViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


