package com.dev.meet.axxessproject.room;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by harishpolusani on 4/8/19.
 */

@Entity(tableName = "comment")
public class Comments {

    @NonNull
    @PrimaryKey()
    private String id;

    @ColumnInfo(name = "comments")
    private String comment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}


