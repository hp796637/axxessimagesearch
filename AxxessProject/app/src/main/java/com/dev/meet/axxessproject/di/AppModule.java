package com.dev.meet.axxessproject.di;

import android.app.Application;
import android.content.Context;
import android.support.v4.content.ContextCompat;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by harishpolusani on 4/6/19.
 */

@Module
public class AppModule {

    private Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return context;
    }
}
