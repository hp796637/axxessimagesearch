package com.dev.meet.axxessproject.di;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.persistence.room.Room;
import android.content.Context;

import com.dev.meet.axxessproject.Constants.UrlUtils;
import com.dev.meet.axxessproject.network.RetrofitApiInterface;
import com.dev.meet.axxessproject.repository.PictureRepository;
import com.dev.meet.axxessproject.room.AppDataBase;
import com.dev.meet.axxessproject.room.CommentsDao;
import com.dev.meet.axxessproject.viewmodel.PictureViewModelFactory;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by harishpolusani on 4/6/19.
 */

@Module
public class PictureModule {
    private Context context;

    public PictureModule(Context context) {
        this.context = context;
    }
    @Provides
    @Singleton
    Gson providesGson() {
        GsonBuilder gsonBuilder = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.setLenient().create();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {

        return new Retrofit.Builder()
                .baseUrl(UrlUtils.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


    }

    @Provides
    @Singleton
    RetrofitApiInterface getRetrofitApiInterface(Retrofit retrofit) {
        return retrofit.create(RetrofitApiInterface.class);
    }

    //Authorization Client-ID 137cda6b5008a7c
    @Provides
    @Singleton
    OkHttpClient getRequestHeader() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();
                Request request = originalRequest.newBuilder().addHeader("Authorization", "Client-ID 137cda6b5008a7c").build();
                return chain.proceed(request);
            }
        });
        return httpClient.build();
    }

    @Provides
    @Singleton
    PictureRepository getRepository(RetrofitApiInterface apiCallInterface) {
        return new PictureRepository(apiCallInterface);
    }

    @Provides
    @Singleton
    ViewModelProvider.Factory getViewModelFactory(PictureRepository myRepository) {
        return new PictureViewModelFactory(myRepository);
    }

    @Provides
    @Singleton
    CompositeDisposable getCompositeDesposible() {
        return new CompositeDisposable();
    }

    @Provides
    @Singleton
    AppDataBase getAppDataBase(Context context) {
        return Room.databaseBuilder(context, AppDataBase.class, UrlUtils.DATABASE_NAME)
                .build();
    }

    @Singleton
    @Provides
     CommentsDao provideShowDao(AppDataBase appDataBase) {
        return appDataBase.commentsDao();
    }

}
