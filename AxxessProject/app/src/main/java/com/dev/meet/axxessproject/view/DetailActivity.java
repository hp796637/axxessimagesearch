package com.dev.meet.axxessproject.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.dev.meet.axxessproject.Constants.Constants;
import com.dev.meet.axxessproject.PictureSearchApplication;
import com.dev.meet.axxessproject.R;
import com.dev.meet.axxessproject.room.Comments;
import com.dev.meet.axxessproject.viewmodel.DeatilActivityViewModel;
import com.dev.meet.axxessproject.viewmodel.PictureActivityViewModel;
import com.dev.meet.axxessproject.viewmodel.PictureViewModelFactory;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by harishpolusani on 4/8/19.
 */

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {


    @BindView(R.id.full_image)
    ImageView imageView;

    @BindView(R.id.comments)
    EditText editText;

    @BindView(R.id.submit)
    Button button;

    @Inject
    PictureViewModelFactory viewModelFactory;
    private DeatilActivityViewModel viewModel;
    private String id;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_image);
        ButterKnife.bind(this);
        ((PictureSearchApplication) getApplication()).getAppComponent().inject(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DeatilActivityViewModel.class);
        Intent intent = getIntent();
        if (intent != null) {
            id = intent.getStringExtra(Constants.id);
            init(intent.getStringExtra(Constants.URL));
            setTitle(intent.getStringExtra(Constants.TITLE));
            if (getSupportActionBar() != null)
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
   getComment method listen to live data for comments
     */
    private void init(String url) {
        viewModel.getComment(this, id).observe(this, new Observer<Comments>() {
            @Override
            public void onChanged(@Nullable Comments comments) {
                if (comments != null)
                    editText.setText(comments.getComment());
            }
        });
        Glide.with(this)
                .load(url).thumbnail(0.1f).placeholder(R.drawable.ic_image_black_place_holder).fitCenter().transition(DrawableTransitionOptions.withCrossFade()).dontTransform().diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
        button.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view == button) {
            if (editText.getText() != null && id != null)
                viewModel.addComment(view.getContext(), editText.getText().toString(), id).observe(this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(@Nullable Boolean isSaved) {
                        if (isSaved) {
                            Toast.makeText(view.getContext(), "Comment Successfully Saved", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(view.getContext(), "Comment not saved", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        }
    }
}
