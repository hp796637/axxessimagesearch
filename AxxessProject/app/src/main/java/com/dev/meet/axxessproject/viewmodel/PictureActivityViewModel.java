package com.dev.meet.axxessproject.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.dev.meet.axxessproject.Constants.Status;
import com.dev.meet.axxessproject.models.DataItem;
import com.dev.meet.axxessproject.paging.PagingSourceFactory;
import com.dev.meet.axxessproject.paging.PictureDataSourceClass;
import com.dev.meet.axxessproject.repository.PictureRepository;

import io.reactivex.disposables.CompositeDisposable;


/**
 * Created by harishpolusani on 4/5/19.
 */

public class PictureActivityViewModel extends ViewModel {

    private PagingSourceFactory pagingSourceFactory;
    private PictureRepository repository;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private LiveData<PagedList<DataItem>> imagesLiveData;
    private LiveData<Status> progressLoadingStatus = new MutableLiveData<>();

    public PictureActivityViewModel(PictureRepository pictureRepository) {
        repository = pictureRepository;
        pagingSourceFactory = new PagingSourceFactory(repository, disposables);
        initPaging();
    }

    private void initPaging() {
        PagedList.Config pagedListConfig =
                new PagedList.Config.Builder()
                        .setEnablePlaceholders(true)
                        .setInitialLoadSizeHint(10)
                        .setPageSize(10).build();

        imagesLiveData = new LivePagedListBuilder<>(pagingSourceFactory, pagedListConfig)
                .build();

        progressLoadingStatus = Transformations.switchMap(pagingSourceFactory.getMutableLiveData(), PictureDataSourceClass::getProgressLiveStatus);

    }


    public LiveData<PagedList<DataItem>> getImages() {
        return imagesLiveData;
    }

    public LiveData<Status> getProgressLoadStatus() {
        return progressLoadingStatus;
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        disposables.clear();
    }
}
