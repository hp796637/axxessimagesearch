package com.dev.meet.axxessproject.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.dev.meet.axxessproject.repository.PictureRepository;

import javax.inject.Inject;

/**
 * Created by harishpolusani on 4/5/19.
 */

public class PictureViewModelFactory implements ViewModelProvider.Factory {

    private PictureRepository repository;

    @Inject
    public PictureViewModelFactory(PictureRepository repository) {
        this.repository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        if (modelClass.isAssignableFrom(PictureActivityViewModel.class)) {
            return (T) new PictureActivityViewModel(repository);
        } else if (modelClass.isAssignableFrom(DeatilActivityViewModel.class)) {
            return (T) new DeatilActivityViewModel(repository);
        }
        throw new IllegalArgumentException("Unknown ViewModelClass");
    }
}
