package com.dev.meet.axxessproject.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.dev.meet.axxessproject.repository.PictureRepository;
import com.dev.meet.axxessproject.room.Comments;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by harishpolusani on 4/8/19.
 */

public class DeatilActivityViewModel extends ViewModel {

    private PictureRepository repository;
    private MutableLiveData<Comments> commentsMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> isCommentSaved = new MutableLiveData<>();
    private final CompositeDisposable disposables = new CompositeDisposable();

    public DeatilActivityViewModel(PictureRepository pictureRepository) {
        repository = pictureRepository;
    }

    public MutableLiveData<Boolean> addComment(Context context, String Comment, String id) {
        Completable.fromAction(() -> repository.addComment(context, Comment, id)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onComplete() {
                isCommentSaved.postValue(true);
            }

            @Override
            public void onError(Throwable e) {
                isCommentSaved.postValue(false);
            }
        });
        return isCommentSaved;
    }

    public MutableLiveData<Comments> getComment(Context context, String id) {
        disposables.add(repository.getComment(context, id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Comments>() {
            @Override
            public void accept(Comments comments) throws Exception {
                commentsMutableLiveData.postValue(comments);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                commentsMutableLiveData.postValue(null);
            }
        }));

        return commentsMutableLiveData;
    }
}
