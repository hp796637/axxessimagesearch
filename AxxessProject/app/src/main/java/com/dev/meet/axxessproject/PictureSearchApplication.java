package com.dev.meet.axxessproject;

import android.app.Application;
import android.content.Context;

import com.dev.meet.axxessproject.di.AppComponent;
import com.dev.meet.axxessproject.di.DaggerAppComponent;
import com.dev.meet.axxessproject.di.PictureModule;

/**
 * Created by harishpolusani on 4/5/19.
 */

public class PictureSearchApplication extends Application {

    AppComponent appComponent;
    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        appComponent = DaggerAppComponent.builder().pictureModule(new PictureModule(this)).build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
