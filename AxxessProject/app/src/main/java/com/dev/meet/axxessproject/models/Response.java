package com.dev.meet.axxessproject.models;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import java.util.List;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Response {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("success")
    private boolean success;

    @SerializedName("status")
    private int status;

    public void setData(List<DataItem> data) {
        this.data = data;
    }

    public List<DataItem> getData() {
        return data;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return
                "Response{" +
                        "data = '" + data + '\'' +
                        ",success = '" + success + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }

//    public static DiffUtil.ItemCallback<DataItem> DIFF_CALLBACK = new DiffUtil.ItemCallback<DataItem>() {
//        @Override
//        public boolean areItemsTheSame(@NonNull DataItem oldResponse, @NonNull DataItem newResponse) {
//            return oldResponse.getId().equals(newResponse.getId());
//        }
//
//        @Override
//        public boolean areContentsTheSame(@NonNull DataItem oldResponse, @NonNull DataItem newResponse) {
//            return oldResponse.equals(newResponse);
//        }
//    };
//
//    @Override
//    public boolean equals(Object obj) {
//        if (obj == this)
//            return true;
//
//        DataItem imageData = (DataItem) obj;
//        return imageData.getId().equals(this.getData());
//    }
}