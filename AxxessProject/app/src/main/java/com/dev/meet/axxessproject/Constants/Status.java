package com.dev.meet.axxessproject.Constants;

/**
 * Created by harishpolusani on 4/8/19.
 */

public enum Status {

    LOADED, LOADING, FAILED,NO_DATA_FOUND;
}
