package com.dev.meet.axxessproject.paging;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import com.dev.meet.axxessproject.Constants.Status;
import com.dev.meet.axxessproject.Constants.UrlUtils;
import com.dev.meet.axxessproject.models.DataItem;
import com.dev.meet.axxessproject.repository.PictureRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by harishpolusani on 4/7/19.
 */

public class PictureDataSourceClass extends PageKeyedDataSource<Integer, DataItem> {


    PictureRepository repository;
    private int index;
    private MutableLiveData<Status> progressLiveStatus;
    private CompositeDisposable compositeDisposable;

    @Inject
    public PictureDataSourceClass(PictureRepository repository, CompositeDisposable compositeDisposable) {
        this.repository = repository;
        progressLiveStatus = new MutableLiveData<>();
        this.compositeDisposable = compositeDisposable;

    }

    public MutableLiveData<Status> getProgressLiveStatus() {
        return progressLiveStatus;
    }


    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, DataItem> callback) {
        progressLiveStatus.postValue(Status.LOADING);
        compositeDisposable.add(repository.fetchPicture(1, UrlUtils.SEARCH).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(rootModel -> {
            progressLiveStatus.postValue(Status.LOADED);
            index++;
            if(rootModel.getData()!=null&&rootModel.getData().size()==0){
                progressLiveStatus.postValue(Status.NO_DATA_FOUND);
            }else {
                callback.onResult(rootModel.getData(), null, index);
            }

        }, throwable -> progressLiveStatus.postValue(Status.FAILED)));
    }


    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, DataItem> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, DataItem> callback) {

        progressLiveStatus.postValue(Status.LOADING);
        compositeDisposable.add(repository.fetchPicture(params.key, UrlUtils.SEARCH).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(rootModel -> {
            progressLiveStatus.postValue(Status.LOADED);
            index++;
            if(rootModel.getData()!=null&&rootModel.getData().size()==0){
                progressLiveStatus.postValue(Status.NO_DATA_FOUND);
            }else {
                callback.onResult(rootModel.getData(), params.key + 1);
            }

        }, throwable -> progressLiveStatus.postValue(Status.FAILED)));

    }
}
