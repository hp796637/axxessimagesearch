package com.dev.meet.axxessproject.di;

import com.dev.meet.axxessproject.PictureSearchApplication;
import com.dev.meet.axxessproject.view.DetailActivity;
import com.dev.meet.axxessproject.view.PictureActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by harishpolusani on 4/6/19.
 */

@Component(modules = {AppModule.class, PictureModule.class})
@Singleton
public interface AppComponent {
    void inject(PictureActivity pictureActivity);

    void inject(PictureSearchApplication application);

    void inject(DetailActivity detailActivity);
}
