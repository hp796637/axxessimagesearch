package com.dev.meet.axxessproject.network;

import com.dev.meet.axxessproject.models.Response;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by harishpolusani on 4/6/19.
 */

public interface RetrofitApiInterface {

    @GET("gallery/search/{page}")
    Single<Response> fetchImages(@Path("page") int page, @Query("q") String search);

}
