package com.dev.meet.axxessproject.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.dev.meet.axxessproject.Constants.UrlUtils;

/**
 * Created by harishpolusani on 4/8/19.
 */
@Database(entities = {Comments.class}, version = 1)
public abstract class AppDataBase extends RoomDatabase {

    public abstract CommentsDao commentsDao();

    private static AppDataBase INSTANCE;

    public static AppDataBase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDataBase.class, UrlUtils.DATABASE_NAME)
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

}
