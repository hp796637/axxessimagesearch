You would be designing a simple image searching app that allows you to add comments to images.
Here are some guidelines to follow:
1. Provide a search field for user input.
2. Make a get request to Imgur API to search for pictures with the term from the search field.
Use https://api.imgur.com/3/gallery/search/1?q=vanilla where q stands for the query string.
Please use the following header to be authorized to make the call.
Authorization Client-ID 137cda6b5008a7c
Docs available at https://apidocs.imgur.com/
3. Display the results for the given search terms, including the image in a grid view.
4. When the user clicks on an image open the image in a new activity with the title. Have the image title
shown in the action bar with a back button.
5. On this screen you have an option to add a comment to the opened image. Note: Don’t use
comments that come in API response. Save and retrieve the comment using a local database. The
comment section and the image should both be visible in the remaining space.
6. Subsequent viewings of the image should retain the comment added previously (if any)
7. Use a Git repository